package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

enum BoradState {
    X,
    O,
    EMPTY
}

public class MainActivity extends AppCompatActivity {

    boolean gameEnded = false;
    BoradState currentPlayer = BoradState.X;
    BoradState[] gameState = {BoradState.EMPTY, BoradState.EMPTY, BoradState.EMPTY, BoradState.EMPTY, BoradState.EMPTY, BoradState.EMPTY, BoradState.EMPTY, BoradState.EMPTY, BoradState.EMPTY};
    int[][] winPositions = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8},
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
            {0, 4, 8}, {2, 4, 6}};
    int[] winMarks = {R.id.row1, R.id.row2, R.id.row3,
            R.id.column1, R.id.column2, R.id.column3,
            R.id.diagonal1, R.id.diagonal2};

    public static int amountOfTurnsPlayed = 0;

    public void onTap(View view) {
        ImageView img = (ImageView) view;
        int tappedImageIndex = Integer.parseInt(img.getTag().toString());
        if (gameEnded) {
            resetGame(view);
            return;
        }

        if (gameState[tappedImageIndex] == BoradState.EMPTY) {
            amountOfTurnsPlayed++;
            gameState[tappedImageIndex] = currentPlayer;

            drawActivePlayerAndChangeTextAndTurn(img);
        }
        // Check if any player has won
        for (int i = 0; i < winPositions.length; i++) {
            int[] winPosition = winPositions[i];
            boolean someoneWon = gameState[winPosition[0]] == gameState[winPosition[1]] &&
                    gameState[winPosition[1]] == gameState[winPosition[2]] &&
                    gameState[winPosition[0]] != BoradState.EMPTY;
            if (someoneWon) {
                int winnerImg;
                gameEnded = true;

                if (gameState[winPosition[0]] == BoradState.X)
                    winnerImg = R.drawable.xwin;
                else
                    winnerImg = R.drawable.owin;

                ImageView status = findViewById(R.id.status);
                status.setImageResource(winnerImg);
                findViewById(winMarks[i]).setVisibility(View.VISIBLE);
                return;
            }
        }

        if (amountOfTurnsPlayed == 9) {
            gameEnded = true;
            ImageView status = findViewById(R.id.status);
            status.setImageResource(R.drawable.nowin);
        }
    }

    private void drawActivePlayerAndChangeTextAndTurn(ImageView selectedImage) {
        int turnImage;
        if (currentPlayer == BoradState.X) {
            selectedImage.setImageResource(R.drawable.x);
            currentPlayer = BoradState.O;
            turnImage = R.drawable.oplay;
        } else {
            selectedImage.setImageResource(R.drawable.o);
            currentPlayer = BoradState.X;
            turnImage = R.drawable.xplay;
        }

        ImageView status = findViewById(R.id.status);
        status.setImageResource(turnImage);
    }

    // reset the game
    public void resetGame(View view) {
        gameEnded = false;
        currentPlayer = BoradState.X;
        amountOfTurnsPlayed= 0;
        for (int i = 0; i < gameState.length; i++) {
            gameState[i] = BoradState.EMPTY;
        }
        for (int i = 0; i < winMarks.length; i++) {
            findViewById(winMarks[i]).setVisibility(View.GONE);
        }

        ((ImageView) findViewById(R.id.imageView0)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView1)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView2)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView3)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView4)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView5)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView6)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView7)).setImageResource(0);
        ((ImageView) findViewById(R.id.imageView8)).setImageResource(0);

        ImageView status = findViewById(R.id.status);
        status.setImageResource(R.drawable.xplay);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}